#include "Arduino.h"
void check_RF24_Network(){

  //-----------------------------------nRF24L01--------------------------------------
  // Pump the network regularly
  // 
  network.update();


  // Is there anything ready for us?
  // Есть ли что-нибудь, готовое к нам?
  while ( network.available() )
  {

    // If so, take a look at it 
    // Если это так, посмотрите на него
    RF24NetworkHeader header;
    network.peek(header);
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Received #%u type %c from 0%o\n\r"),millis(),header.id,header.type,header.from_node);
#endif
    // Dispatch the message to the correct handler.
    // Отправить сообщение на правильный обработчик.
    switch (header.type)
    {
    case 'M':
      handle_M(header);
      break;
    case 'N':
      handle_N(header);
      break;
    case 'I':
      handle_I(header);
      break;      
    default:
#ifdef DEDUG
      printf_P(PSTR("*** WARNING *** Unknown message type %c\n\r"),header.type);
#endif
      network.read(header,0,0);
      break;
    };
  }
  
  
  
  
  // Send a ping to the next node every 'interval' ms
  // Отправить пинг к следующему узлу каждые 'Interval' ms
  //  unsigned long now = millis();
  // if ( now - last_time_sent >= interval )
  //  {


  //  last_time_sent = now;

  // Who should we send to?
  // By default, send to base
  // Кому мы должны отправить?
  // По умолчанию, отправить на базу
  uint16_t to = 00;
  /*
    // Or if we have active nodes,
   // Или, если у нас есть активные узлы,
   if ( num_active_nodes )
   {
   // Send to the next active node
   // Отправить на следующий активном узле
   to = active_nodes[next_ping_node_index++];
   
   // Have we rolled over?
   // Разве мы перевернулся?
   if ( next_ping_node_index > num_active_nodes )
   {
   // Next time start at the beginning
   // В следующий раз начать с самого начала
   next_ping_node_index = 0;
   
   // This time, send to node 00.
   // На этот раз, отправить на узел 00.
   to = 00;
   }
   }
   */
  bool ok;

  // Normal nodes send a 'M' ping
  // Нормальные узлы отправить 'T' Пинг
  int i=0;
  do{
  if ( i<=Repeat_send ){  
  if ( this_node.address > 00 || to == 00 ){

    LED_ON;
    ok = Send_Value_To_Mega();
    LED_OFF;
  }
  // Base node sends the current active nodes out
  // База узел посылает текущие активные узлы вне
  else
  {
    LED_ON;
    ok = send_N(to);
    LED_OFF;
  }


 
  // Notify us of the result
  // Сообщите нам о результате
  if (ok)
  {
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Send ok\n\r"),millis());
#endif
  }
  else
  {
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Send failed\n\r"),millis());
#endif
    // Try sending at a different time next time
    // Попробуйте отправить в другое время в следующий раз
    //     last_time_sent -= 100;
    
  }
  }
  i++;
  }
 while (!ok ||  Repeat_send == i) ;
  //  }
  //   uint16_t to = 00;  

  // Listen for a new node address
  // Прислушайтесь к новому адресу узла
  nodeconfig_listen();
  //-----------------------------------nRF24L01--------------------------------------

}








