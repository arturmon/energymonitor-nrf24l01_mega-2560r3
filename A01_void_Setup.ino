void setup() {
  // put your setup code here, to run once:
  pinMode(LEDpin, OUTPUT);
  pinMode(RELEpin, OUTPUT);

  digitalWrite(RELEpin, LOW);
  EnergyMonitor.RELE_1==0;
  
#ifdef DEDUG
  printf_begin();
  Serial.begin(57600);

  Serial.println("emonTx V3 Real Power Example node MEGA2560R3\n");
  Serial.println("Dallas Temperature Control Library - Async Demo\n");
  Serial.println("Dallas Library Version: ");
  Serial.println(DALLASTEMPLIBVERSION);
#endif

  //----------------------------------------самое главное!!!!!!!!Калибровка тут----------------------------------------------
  // (ADC input, calibration, phase_shift) 223.807 подбираем эксперемнтальным путем по показаниям мультиметра
  ct1.voltage(1, 223.807, 1.7);                                
  ct2.voltage(2, 223.807, 1.7);                                
  ct3.voltage(3, 223.807, 1.7);

  // Calibration factor = CT ratio / burden resistance = (100A / 0.05A) / 33 Ohms = 60.606
  //69 Ohms /28,985507246376811594202898550725
  ct1.current(0, 28.985);
  ct2.current(6, 28.985);                                     
  ct3.current(7, 28.985);
  //----------------------------------------самое главное!!!!!!!!Калибровка тут----------------------------------------------

  //--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------
  lcd.begin(16,2);         // (20,4) initialize the lcd for 20 chars 4 lines and turn on backlight
  lcd.createChar(1, temp_cel);
  lcd.createChar(2, Rele_is_on);
  lcd.createChar(3, Rele_is_off);
  // ------- Быстрые 3 мигания подсветки  -------------
  for(int i = 0; i< 3; i++)
  {
    lcd.backlight();
    digitalWrite(LEDpin, HIGH);
    delay(250);
    lcd.noBacklight();
    digitalWrite(LEDpin, LOW);
    delay(250);
  }
  lcd.backlight(); // finish with backlight on 

  lcd.setCursor(2,0);
  //lcd.print("Loading.");
  lcd.print("Loading.");
  //--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------


  sensors.begin();
  sensors.getAddress(tempDeviceAddress, 0);
  sensors.setResolution(tempDeviceAddress, resolution);

  sensors.setWaitForConversion(false);
  sensors.requestTemperatures();
  delayInMillis = 750 / (1 << (12 - resolution)); 
  lastTempRequest = millis(); 


  //--------------------------------Настраиваем таймеры---------------------------------------

  tmr1.event(&elapsed1);
  tmr2.event(&elapsed2);  
  tmr3.event(&elapsed3);
  tmr1.start();
  tmr2.start();
  tmr3.start();

  //--------------------------------Настраиваем таймеры---------------------------------------


  //-----------------------------------nRF24L01--------------------------------------
  //Serialprint("\n\rRF24Network Initial\n\r");
  //
  // Вытащить адрес узла из EEPROM
  //

  // Which node are we?
  // Какой узел мы?
  this_node = nodeconfig_read();


  //
  // Bring up the RF network
  // Вызовите RF сеть

  SPI.begin();
  radio.begin();
  network.begin(/*channel*/ 100, /*node address*/ this_node.address );

  bool ok;

  ok = Inicial_To_Mega();

  if (ok)
  {
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Inicial ok\n\r"),millis());
#endif
  }
  else
  {
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Inicial failed\n\r"),millis());
#endif
  }

  //-----------------------------------nRF24L01--------------------------------------



  delay(2000); 
  lcd.clear();
}













