void elapsed1()
{

#ifdef DEDUG
  printf_P(PSTR("-----------------TIMER-1(tmr1)----------------- "));  
  printf_P(PSTR("%lu"),millis()/1000);
  printf_P(PSTR("c \n")); 
#endif  
  Update_Variable();
#ifdef DEDUG_SERIAL_POWER  
  Power_Out_to_Serial(); 
#endif
}

void elapsed2()
{

#ifdef DEDUG
  printf_P(PSTR("-----------------TIMER-2(tmr2)----------------- "));  
  printf_P(PSTR("%lu"),millis()/1000);
  printf_P(PSTR("c \n"));
#endif  
  check_RF24_Network();

  Out_to_LCD_Power();
}

void elapsed3()
{

#ifdef DEDUG
  printf_P(PSTR("-----------------TIMER-3(tmr3)----------------- "));   
  printf_P(PSTR("%lu"),millis()/1000);
  printf_P(PSTR("c \n"));
#endif  
  Get_18B20_Data();
}





bool Inicial_To_Mega(){
  //Долбим Мегу пока не произойдет акта инициализации
  //тут же отправляем список датчиков
  RF24NetworkHeader header(network.parent(),'I');
#ifdef DEDUG
  printf_P(PSTR("%lu: APP Sending type-%c to 0%o...\n\r"),millis(),header.type,header.to_node);
#endif  
  while ( ! network.write(header,NULL,0) ){
#ifdef DEDUG
    printf_P(PSTR("Failed.\r\n")); 
#endif  
    RF24NetworkHeader header(network.parent(),'I');
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Sending type-%c to 0%o...\n\r"),millis(),header.type,header.to_node);    
#endif  
    uint16_t to = 00;
    bool ok;
    LED_ON;
    for (int i=0;i>=Number_of_Sensors;i++){
      F_tt.type=1;//Тип Инициализации 
      F_tt.number=Number_of_Sensors;//текущий датчик
      F_tt.value=Type_Sensors[Number_of_Sensors];//Значения датчика
      ok = send_I(to,F_tt.type,F_tt.number,F_tt.value);   
    }
    //Подтверждение инициализации
    F_tt.type=250; 
    F_tt.number=250;
    F_tt.value=250;
    ok = send_I(to,F_tt.type,F_tt.number,F_tt.value);     
  }
  LED_OFF; 
}


void  Update_Variable(){

  EnergyMonitor.realPower_ct1=ct1.realPower;
  EnergyMonitor.apparentPower_ct1=ct1.apparentPower;
  EnergyMonitor.Irms_ct1=ct1.Irms;
  EnergyMonitor.Vrms_ct1=ct1.Vrms;

  EnergyMonitor.realPower_ct2=ct2.realPower;
  EnergyMonitor.apparentPower_ct2=ct2.apparentPower;
  EnergyMonitor.Irms_ct2=ct2.Irms;
  EnergyMonitor.Vrms_ct2=ct2.Vrms;

  EnergyMonitor.realPower_ct3=ct3.realPower;
  EnergyMonitor.apparentPower_ct3=ct3.apparentPower;
  EnergyMonitor.Irms_ct3=ct3.Irms;
  EnergyMonitor.Vrms_ct3=ct3.Vrms;

  EnergyMonitor.DS18B20=temperature = sensors.getTempCByIndex(0);


  if (EnergyMonitor.RELE_1==1)   digitalWrite(RELEpin, HIGH);
  else if (EnergyMonitor.RELE_1==0) digitalWrite(RELEpin, LOW);
  //EnergyMonitor.Passes_the_test=;


}

bool Send_Value_To_Mega(){
  //Всегда на MEga2560 R3
  uint16_t to = 00;

  bool ok;
  //отправляем температуру

  F_tt.type=11;//Тип датчика    ds18b20	11
  F_tt.number=1;//Номер датчика
  F_tt.value=EnergyMonitor.DS18B20;//Значения датчика
  ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);

  //отправляем состояние реле

  F_tt.type=24;//Тип датчика    RELE	24
  F_tt.number=2;//Номер датчика
  F_tt.value=EnergyMonitor.RELE_1;//Значения датчика
  ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);


  //отправляем показания первого канала
  if (ok){
    F_tt.type=16;//Тип датчика    realPower	16
    F_tt.number=3;//Номер датчика
    F_tt.value=EnergyMonitor.realPower_ct1;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value); 
  }
  //------------------------------------------------  
  if (ok){
    F_tt.type=17;//Тип датчика    apparentPower	17
    F_tt.number=4;//Номер датчика
    F_tt.value=EnergyMonitor.apparentPower_ct1;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);  
  } 
  //------------------------------------------------ 
  if (ok){ 
    F_tt.type=14;//Тип датчика    I	14
    F_tt.number=5;//Номер датчика
    F_tt.value=EnergyMonitor.Irms_ct1;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);  
  }
  //------------------------------------------------  
  if (ok){
    F_tt.type=15;//Тип датчика    V	15
    F_tt.number=6;//Номер датчика
    F_tt.value=EnergyMonitor.Vrms_ct1;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value); 
  } 



  //отправляем показания второго канала
  if (ok){
    F_tt.type=16;//Тип датчика    realPower	16
    F_tt.number=7;//Номер датчика
    F_tt.value=EnergyMonitor.realPower_ct2;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value); 
  }
  //------------------------------------------------  
  if (ok){
    F_tt.type=17;//Тип датчика    apparentPower	17
    F_tt.number=8;//Номер датчика
    F_tt.value=EnergyMonitor.apparentPower_ct2;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);  
  } 
  //------------------------------------------------  
  if (ok){
    F_tt.type=14;//Тип датчика    I	14
    F_tt.number=9;//Номер датчика
    F_tt.value=EnergyMonitor.Irms_ct2;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value); 
  } 
  //------------------------------------------------  
  if (ok){
    F_tt.type=15;//Тип датчика    V	15
    F_tt.number=10;//Номер датчика
    F_tt.value=EnergyMonitor.Vrms_ct2;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);   
  }


  //отправляем показания Третьего канала
  if (ok){
    F_tt.type=16;//Тип датчика    realPower	16
    F_tt.number=11;//Номер датчика
    F_tt.value=EnergyMonitor.realPower_ct3;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value); 
  }
  //------------------------------------------------ 
  if (ok){ 
    F_tt.type=17;//Тип датчика    apparentPower	17
    F_tt.number=12;//Номер датчика
    F_tt.value=EnergyMonitor.apparentPower_ct3;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);
  }  
  //------------------------------------------------ 
  if (ok){ 
    F_tt.type=14;//Тип датчика    I	14
    F_tt.number=13;//Номер датчика
    F_tt.value=EnergyMonitor.Irms_ct3;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);
  }  
  //------------------------------------------------ 
  if (ok){ 
    F_tt.type=15;//Тип датчика    V	15
    F_tt.number=14;//Номер датчика
    F_tt.value=EnergyMonitor.Vrms_ct3;//Значения датчика
    ok = send_M(to,F_tt.type,F_tt.number,F_tt.value);  
  }


}








