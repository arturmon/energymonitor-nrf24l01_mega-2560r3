void Power_Out_to_Serial(){
#ifdef DEDUG_SERIAL_POWER

  printf_P(PSTR("c1_realPower="));
  Serial.print(ct1.realPower);
  printf_P(PSTR(" c1_apparentPower="));
  Serial.print(ct1.apparentPower);
  printf_P(PSTR(" c1_Irms="));
  Serial.print(ct1.Irms);
  printf_P(PSTR(" c1_Vrms="));
  Serial.println(ct1.Vrms);

  printf_P(PSTR("c2_realPower="));
  Serial.print(ct2.realPower);
  printf_P(PSTR(" c2_apparentPower="));
  Serial.print(ct2.apparentPower);
  printf_P(PSTR(" c2_Irms="));
  Serial.print(ct2.Irms);
  printf_P(PSTR(" c2_Vrms="));
  Serial.println(ct2.Vrms);

  printf_P(PSTR("c3_realPower="));
  Serial.print(ct3.realPower);
  printf_P(PSTR(" c3_apparentPower="));
  Serial.print(ct3.apparentPower);
  printf_P(PSTR(" c3_Irms="));
  Serial.print(ct3.Irms);
  printf_P(PSTR(" c3_Vrms="));
  Serial.println(ct3.Vrms);
  Serial.println();
#endif
}


