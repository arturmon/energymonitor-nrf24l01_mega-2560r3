#include "Arduino.h"


// type - Тип датчика
// number - Номер датчика
// value - Значения датчика
bool send_M(uint16_t to,unsigned int type,unsigned int number,float value)
{

  payload_t payload = { 
    type, number, float(value)        };

  RF24NetworkHeader header(/*to node*/ to, /*type*/ 'M' /*Time*/); 
#ifdef DEDUG
  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP +++++ %lu to 0%o...\n\r"),millis(),payload,to);
#endif
  return network.write(header,&payload,sizeof(payload));
}

// type - Тип датчика
// number - Номер датчика
// value - Значения датчика
bool send_I(uint16_t to,unsigned int type,unsigned int number,float value)
{

  payload_t payload = { 
    type, number, float(value)        };

  RF24NetworkHeader header(/*to node*/ to, /*type*/ 'I' /*Time*/); 
#ifdef DEDUG
  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP +++++ %lu to 0%o...\n\r"),millis(),payload,to);
#endif
  return network.write(header,&payload,sizeof(payload));
}



/**
 * Send an 'N' message, the active node list
 * Отправить сообщение 'N', активный список узлов
 */
bool send_N(uint16_t to)
{
  RF24NetworkHeader header(/*to node*/ to, /*type*/ 'N' /*Time*/);
#ifdef DEDUG
  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP Sending active nodes to 0%o...\n\r"),millis(),to);
#endif
  return network.write(header,active_nodes,sizeof(active_nodes));
}



/**
 * Handle a 'M' message
 *
 * Add the node to the list of active nodes
 * Добавить узел в список активных узлов
 */
void handle_M(RF24NetworkHeader& header)
{
  // The 'M' message is just a ulong, containing the time
  // Сообщение "M" находится всего в ulog, содержащий время
  unsigned long message;
  network.read(header,&message,sizeof(unsigned long));
#ifdef DEDUG
  printf_P(PSTR("%lu: APP Received %lu from 0%o\n\r"),millis(),message,header.from_node);
#endif
  // If this message is from ourselves or the base, don't bother adding it to the active nodes.
  // Если это сообщение является от самих себя или основания, не беспокойтесь добавив его в активных узлов.



  if (header.from_node==00){//проверяем если от основания то


    payload_t payload;
    network.read(header,&payload,sizeof(payload));
    if (payload.number == 2 && payload.type == 24){ //проверяем что это второй датчик и сравниваем его ID 
      // теперь проверяем что пришло и устанавливаем состояние переменной реле в структуре
      // состояние реле само обновится при следующейм проходе функции Update_Variable()
      if (payload.value == 0) {
        EnergyMonitor.RELE_1 = 0;
      }
      else {
        EnergyMonitor.RELE_1 = 1;
      }
    }  


  } 


  if ( header.from_node != this_node.address || header.from_node > 00 )
    add_node(header.from_node);
}

/**
 * Handle an 'N' message, the active node list
 * Обращение сообщение 'N', активный список узлов
 */
void handle_N(RF24NetworkHeader& header)
{
  static uint16_t incoming_nodes[max_active_nodes];

  network.read(header,&incoming_nodes,sizeof(incoming_nodes));
#ifdef DEDUG
  printf_P(PSTR("%lu: APP Received nodes from 0%o\n\r"),millis(),header.from_node);
#endif
  int i = 0;
  while ( i < max_active_nodes && incoming_nodes[i] > 00 )
    add_node(incoming_nodes[i++]);
}


/**
 * Handle an 'I' message, the active node list
 * Обращение сообщение 'I', Тут отбрабатываем полученный список нод
 */
void handle_I(RF24NetworkHeader& header)
{
  //Тут отбрабатываем полученный список нод
#ifdef DEDUG
  printf_P(PSTR("%lu: APP Received nodes from 0%o\n\r"),millis(),header.from_node);
#endif

}

/**
 * Add a particular node to the current list of active nodes
 * Добавить конкретный узел в текущий список активных узлов
 */
void add_node(uint16_t node)
{
  // Do we already know about this node?
  // Разве мы уже знаем об этой узла?
  short i = num_active_nodes;
  while (i--)
  {
    if ( active_nodes[i] == node )
      break;
  }
  // If not, add it to the table
  // Если нет, добавьте его к столу
  if ( i == -1 && num_active_nodes < max_active_nodes )
  {
    active_nodes[num_active_nodes++] = node; 
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Added 0%o to list of active nodes.\n\r"),millis(),node);
#endif
  }
}





