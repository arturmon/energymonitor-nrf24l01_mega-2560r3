/*
float Get_18B20_Data(){	 
 byte DSdata[2];
 ds.reset();
 ds.write(0xCC);
 ds.write(0x44);
 delay(1000);
 ds.reset();
 ds.write(0xCC);
 ds.write(0xBE);
 DSdata[0] = ds.read();
 DSdata[1] = ds.read();
 int Temp = (DSdata[1] << 8) + DSdata[0];
 return (float) Temp / 16;
 }
 */

void Get_18B20_Data(){
  if (millis() - lastTempRequest >= delayInMillis) // достаточно долго ждали?
  {

   // digitalWrite(LEDpin, LOW);
#ifdef DEDUG_SERIAL_TEMPERATURE
    printf_P(PSTR(" Temperature: "));
#endif
    temperature = sensors.getTempCByIndex(0);
#ifdef DEDUG_SERIAL_TEMPERATURE
    Serial.println(temperature, resolution - 8); 
    printf_P(PSTR("  Resolution: "));
    printf_P(PSTR("%d\n"),resolution); 
    printf_P(PSTR("Idle counter: "));
    printf_P(PSTR("%d\n"),idle);     
    printf_P(PSTR("\n")); 
#endif
    idle = 0; 

    // Сразу после извлечения температуру мы просим нового образца
    // В асинхронном модус
    // Для демо мы позволяем изменение разрешения, чтобы показать различия
    resolution++;
    if (resolution > 12) resolution = 9;

    sensors.setResolution(tempDeviceAddress, resolution);
    sensors.requestTemperatures(); 
    delayInMillis = 750 / (1 << (12 - resolution));
    lastTempRequest = millis(); 
  }

 // digitalWrite(LEDpin, HIGH);
  // мы можем сделать полезные вещи здесь
  // для демо мы просто подсчитать время простоя в Millis
  delay(1);
  idle++;
}




