#include <FastIO.h>
#include <I2CIO.h>
#include <LCD.h>
#include <LiquidCrystal.h>
#include <LiquidCrystal_I2C.h>
#include "printf.h"

#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#include "EmonLib.h"             // Include Emon Library http://openenergymonitor.org

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <RF24Network.h>
#include "A04_Node_Config_NF24.h"
#include "A05_Send_and_Handle_NF24.h"

#include <longtime.h>

#include <OneWire.h>

#define DEDUG; //Закоментируйте когда не нужен вывод в сериал, экономит флеш память
//#define DEDUG_SERIAL_POWER;
//#define DEDUG_SERIAL_TEMPERATURE;



OneWire ds(4);      //Создаем датчик DS18B20 на 3 пине
DallasTemperature sensors(&ds);

DeviceAddress tempDeviceAddress;

int  resolution = 12;
unsigned long lastTempRequest = 0;
int  delayInMillis = 0;
float temperature = 0.0;
int  idle = 0;


EnergyMonitor ct1, ct2, ct3; 

const int LEDpin = 5;
const int RELEpin = 8;

#define LED_ON digitalWrite(LEDpin, HIGH);
#define LED_OFF digitalWrite(LEDpin, LOW);

/**
 * Convenience class for handling LEDs.  Handles the case where the
 * LED may not be populated on the board, so always checks whether
 * the pin is valid before setting a value.
 */
/*
class LED
 {
 private:
 int pin;
 public:
 LED(int _pin): 
 pin(_pin)
 {
 if (pin > 0)
 {
 pinMode(pin,OUTPUT);
 digitalWrite(pin,LOW);
 }
 }
 void write(bool state) const
 {
 if (pin > 0)
 digitalWrite(pin,state?HIGH:LOW);
 }
 void operator=(bool state)
 {
 write(state);
 }
 
 };
 */


typedef struct{
  float realPower_ct1;
  float realPower_ct2;
  float realPower_ct3;

  float apparentPower_ct1;
  float apparentPower_ct2;
  float apparentPower_ct3;

  float Irms_ct1;
  float Irms_ct2;
  float Irms_ct3;

  float Vrms_ct1;
  float Vrms_ct2;
  float Vrms_ct3;

  float DS18B20;

  boolean RELE_1;

  int Passes_the_test;
}
F_t;

F_t EnergyMonitor;

//type	5	boolean
//	6	int
//	7	float

const int Number_of_Sensors =15;
int Type_Sensors[Number_of_Sensors]={
  7,7,7,7,7,7,7,7,7,7,5,6};

struct payload_t
{
  unsigned int type;          //<< Тип датчика и порядковый номер
  unsigned int number;        //<< Номер датчика
  float value;                //<< Значения датчика
}
F_tt;

//-----------------------------------nRF24L01--------------------------------------

// nRF24L01(+) radio using the Getting Started board
RF24 radio(9,10); //rf_ce,rf_csn
RF24Network network(radio);

// Наш адрес узла
//uint16_t this_node;

// Our node configuration 
eeprom_info_t this_node;

uint8_t has_recent_system_config = false;

// Number of packets we've failed to send since we last sent one
// successfully
//uint16_t lost_packets = 0;
uint16_t Repeat_send = 3;


// Message buffer space
uint8_t message[32];
void send_finder_request(void);

// Задержка менеджер отправить пинги регулярно
//const unsigned long interval = 2000; // ms
//unsigned long last_time_sent;

// Массив узлов известных нам
const short max_active_nodes = 10;
uint16_t active_nodes[max_active_nodes];
short num_active_nodes = 0;
short next_ping_node_index = 0;

// Прототипы для функций для отправки и обработки сообщений
bool send_M(uint16_t to,unsigned int type,unsigned int number,float value);
bool send_N(uint16_t to);
bool send_I(uint16_t to,unsigned int type,unsigned int number,float value);
void handle_M(RF24NetworkHeader& header);
void handle_N(RF24NetworkHeader& header);
void handle_I(RF24NetworkHeader& header);
void add_node(uint16_t node);





//-----------------------------------nRF24L01--------------------------------------
/*
// Variables will change:
 int ledState = LOW;             // ledState используется для установки светодиод
 long previousMillis = 0;        //будет хранить последний раз индикатор был обновлен
 
 // в последующих переменные длинный, потому что время, измеряемое в миллисекундах,
 // быстро станет больше, чем число может храниться в Int.
 long interval = 500;           // Интервал, с которым мигать (в миллисекундах)
 */
//--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------
// set the LCD address to 0x27 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
//http://hmario.home.xs4all.nl/arduino/LiquidCrystal_I2C/LiquidCrystal_I2C.zip
uint8_t Adress_LCD_I2C = 0x27;
LiquidCrystal_I2C lcd(Adress_LCD_I2C, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
long count_Print_LCD;
uint8_t temp_cel[8] =
{
  B00111,
  B00101,
  B00111,
  B00000,
  B00000,
  B00000,
  B00000
};
byte Rele_is_on[8] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111
}; 
byte Rele_is_off[8] = {
  B11111,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B11111
}; 
//--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------

//------------------------------StreamPrint------------------------------------------
#define Serialprint(format, ...) StreamPrint_progmem(Serial,PSTR(format),##__VA_ARGS__)
#define Streamprint(stream,format, ...) StreamPrint_progmem(stream,PSTR(format),##__VA_ARGS__)
//------------------------------StreamPrint------------------------------------------

//-----------------------------------Устанавливаем таймеры-----------------------------------

CLongTimer tmr1(2000);//через какое время будет проиведен следующий опрос elapsed1
CLongTimer tmr2(5000);//через какое время будет проиведен следующий опрос elapsed2
CLongTimer tmr3(10000);//через какое время будет проиведен следующий опрос elapsed3 
//-----------------------------------Уствнвыливаем таймеры-----------------------------------

